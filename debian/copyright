Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: cubew
Source: https://www.scalasca.org/software/cube-4.x/download.html

Files: *
Copyright: Copyright (c) 1998-2022 Forschungszentrum Juelich GmbH, Germany
           Copyright (c) 2009-2015 German Research School for Simulation Sciences GmbH,
                                   Juelich/Aachen, Germany
           All rights reserved.
License: BSD-3-Clause

Files: common/*
Copyright: Copyright (c) 2009-2014 RWTH Aachen University, Germany
           Copyright (c) 2009-2013 Gesellschaft fuer numerische Simulation mbH
                                   Braunschweig, Germany
           Copyright (c) 2009-2017 Technische Universitaet Dresden, Germany
           Copyright (c) 2009-2013 University of Oregon, Eugene, USA
           Copyright (c) 1998-2020 Forschungszentrum Juelich GmbH, Germany
           Copyright (c) 2009-2015 German Research School for Simulation Sciences GmbH,
                                   Juelich/Aachen, Germany
           Copyright (c) 2009-2013 Technische Universitaet Muenchen, Germany
License: BSD-3-Clause

Files: common/utils/test/cutest/*
Copyright: Copyright (c) 2003 Asim Jalis
License: zlib/libpng
 This software is provided 'as-is', without any express or implied
 warranty. In no event will the authors be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must not
 claim that you wrote the original software. If you use this software in
 a product, an acknowledgment in the product documentation would be
 appreciated but is not required.
 .
 2. Altered source versions must be plainly marked as such, and must not
 be misrepresented as being the original software.
 .
 3. This notice may not be removed or altered from any source
 distribution.

License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
 * Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
 .
 * Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
 .
 * Neither the names of
     the Forschungszentrum Juelich GmbH,
     the German Research School for Simulation Sciences GmbH,
   nor the names of their contributors may be used to endorse or promote
   products derived from this software without specific prior written
   permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
